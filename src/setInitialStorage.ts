export default function setInitialStorage(): void {
  const key = 'likedMovies';
  if (!Object.keys(localStorage).includes(key)) {
    localStorage[key] = JSON.stringify([])
  }
}