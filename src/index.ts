import setInitialStorage from './setInitialStorage';
import renderMovieAlbum from './renderFuncs/renderMovieAlbum';
import renderLikedMovies from './renderFuncs/renderLikedMovies';
import renderRandomMovie from './renderFuncs/renderRandomMovie';

export async function render(): Promise<void> {
  const navBar = document.querySelector('.navbar-toggler');

  setInitialStorage();
  renderRandomMovie();
  renderMovieAlbum();

  navBar?.addEventListener('click', renderLikedMovies);
}
