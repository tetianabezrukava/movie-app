import {
  ITransformData,
  IFetchData,
  IFetchResult,
  IMovie,
  IMovieForRandom
} from './typingHelpers/interfaces';

export default class ApiService {
  protected apiBase = 'https://api.themoviedb.org/3/';
  protected apiKey = 'api_key=789af2c99338330fb4e287bad0137a36';
  protected endPointPopular = `${this.apiBase}movie/popular?${this.apiKey}&page=`;
  protected endPointTopRated = `${this.apiBase}movie/top_rated?${this.apiKey}&page=`;
  protected endPointUpcoming = `${this.apiBase}movie/upcoming?${this.apiKey}&page=`;
  protected endPointSearch = `${this.apiBase}search/movie?${this.apiKey}&page=`;
  protected posterBase = 'https://image.tmdb.org/t/p/w500';
  protected backdropBase = 'https://image.tmdb.org/t/p/original';

  fetchData = async<T>(url: string): Promise<T> => {
    const res = await fetch(url);
    if(!res.ok) {
      throw new Error(`Could not fetch ${url}, received ${res.status}`);
    }

    return await res.json()
  }

  getMovies = async(url: string): Promise<IFetchResult> => {
    const res = await this.fetchData<IFetchData>(url);
    return (
      {
        totalPages: res.total_pages,
        movies: res.results.map(this.transformMovie)
      }
    )
  }

  getPopular = (page: number): Promise<IFetchResult> => {
    const url = `${this.endPointPopular}${page}`;
    return this.getMovies(url)
  }

  getTopRated = (page: number): Promise<IFetchResult> => {
    const url = `${this.endPointTopRated}${page}`;
    return this.getMovies(url)
  }

  getUpcoming = (page: number): Promise<IFetchResult> => {
    const url = `${this.endPointUpcoming}${page}`;
    return this.getMovies(url)
  }

  getBySearch = (page: number, query: string | undefined): Promise<IFetchResult> => {
    const url = `${this.endPointSearch}${page}&query=${query}`;
    return this.getMovies(url)
  }

  getMovieById = async(id: number): Promise<IMovie> => {
    const url = `${this.apiBase}movie/${id}?${this.apiKey}`;
    const res = await this.fetchData<ITransformData>(url);
    return this.transformMovie(res)
  }

  getMoviesForRandom = async() => {
    const url = `${this.endPointPopular}1`;
    const res = await this.fetchData<IFetchData>(url);
    return res.results.map(this.transformMoviesForRandom)
  }

  protected transformMovie = ({ id, overview, release_date, poster_path }: ITransformData) => {
    return {
      id,
      overview,
      poster: poster_path ? `${this.posterBase}${poster_path}` : poster_path,
      release: release_date || 'unknown'
    }
  }

  protected transformMoviesForRandom = ({title, overview, poster_path, backdrop_path}: IMovieForRandom) => {
    return {
      title,
      overview,
      backdrop: backdrop_path ? `${this.backdropBase}${backdrop_path}` : `${this.backdropBase}${poster_path}`
    }
  }
}