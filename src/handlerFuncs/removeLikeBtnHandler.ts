import renderLikedMovies from '../renderFuncs/renderLikedMovies';

export default function removeLikeBtnHandler(id: string): void {
  const album = document.getElementById('film-container');
  const removedMovie = album?.querySelector(`.movie-${id}`);
  if (removedMovie) {
    const likeBtn = removedMovie.querySelector('svg');
    if (likeBtn) {
        likeBtn.style.fill = "#ff000078";
    }
  }

  const likedMoviesArr = JSON.parse(localStorage.likedMovies);
  const searchMovie = likedMoviesArr.indexOf(id)
  const newArr = [
    ...likedMoviesArr.slice(0, searchMovie),
    ...likedMoviesArr.slice(searchMovie + 1)
  ]

  localStorage.likedMovies = JSON.stringify(newArr);
  renderLikedMovies();
}