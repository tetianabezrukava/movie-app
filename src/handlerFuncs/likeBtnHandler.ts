export default function likeBtnHandler(id: string, btn: SVGSVGElement | undefined): void {
  const likedMoviesArr = JSON.parse(localStorage.likedMovies);
  let newArr;
  if(likedMoviesArr.includes(id)) {
    if (btn) {
      btn.style.fill = '#ff000078';
    }

    const searchMovie = likedMoviesArr.indexOf(id)
    newArr = [
      ...likedMoviesArr.slice(0, searchMovie),
      ...likedMoviesArr.slice(searchMovie + 1)
    ]
    } else {
      if (btn) {
        btn.style.fill = 'red';
      }

      newArr = [...likedMoviesArr, id];
  }

  localStorage.likedMovies = JSON.stringify(newArr)
}