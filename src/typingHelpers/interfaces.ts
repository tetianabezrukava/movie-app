interface ITransformData {
	id: number;
	overview: string;
	release_date: string;
	poster_path: string
}

interface IFetchData {
	results: [];
	total_pages: number
}

interface IMovie {
	id: number;
	overview: string;
	poster: string | null;
	release: string
}

interface  IMovieForRandom {
	title: string;
	overview: string;
	poster_path: string;
	backdrop_path: string
}

interface IFetchResult {
	totalPages: number;
	movies: IMovie[]
}

export {
	ITransformData,
	IFetchData,
	IFetchResult,
	IMovie,
	IMovieForRandom
}