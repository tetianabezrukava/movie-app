import { IFetchResult } from './interfaces';

type HeartBtnHandler = (id: string, btn?: SVGSVGElement | undefined) => void;
type FetchFunction = (page: number, query?: string ) => Promise<IFetchResult>;


export {
	HeartBtnHandler,
	FetchFunction
}