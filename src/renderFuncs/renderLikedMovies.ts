import ApiService from "../apiService";
import createCard from '../createElementFuncs/createCard';
import removeLikeBtnHandler from '../handlerFuncs/removeLikeBtnHandler';

export default function renderLikedMovies(): void {
  const service = new ApiService();
  const favoriteMoviesContainer = document.getElementById('favorite-movies');
  if (favoriteMoviesContainer) {
    favoriteMoviesContainer.innerHTML = '';
  }

  const likedMoviesArr = JSON.parse(localStorage.likedMovies);

  likedMoviesArr.map((movieId: number) => {
    service.getMovieById(movieId)
      .then((movie) => {
        const classes = ['col-12', 'p-2']
        const card = createCard(movie, classes, removeLikeBtnHandler);
        favoriteMoviesContainer?.appendChild(card);
      })
      .catch(err => console.log(err));
  })
}