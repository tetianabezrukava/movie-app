import ApiService from '../apiService';
import renderMovies from './renderMovies';

export default function renderMovieAlbum(): void {
  const service = new ApiService();
  const { getPopular, getUpcoming, getTopRated, getBySearch } = service;
  const popularBtn = document.getElementById('popular');
  const upcomingBtn = document.getElementById('upcoming');
  const topRatedBtn = document.getElementById('top_rated');
  const searchForm = document.querySelector('form');
  const search = document.getElementById('search') as HTMLInputElement;

  renderMovies(getPopular)

  popularBtn?.addEventListener('click', () => renderMovies(getPopular));
  upcomingBtn?.addEventListener('click', () => renderMovies(getUpcoming));
  topRatedBtn?.addEventListener('click', () => renderMovies(getTopRated));

  searchForm?.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchQuery = search.value.trim()
    if(searchQuery) {
      renderMovies(getBySearch, searchQuery)
      search.value = ''
    }
  })
}