import createCard from '../createElementFuncs/createCard';
import createLoadMoreBtn from '../createElementFuncs/createLoadMoreBtn';
import likeBtnHandler from '../handlerFuncs/likeBtnHandler';
import { IFetchResult } from '../typingHelpers/interfaces';
import { FetchFunction } from '../typingHelpers/types';

export default function renderMovies(func: FetchFunction, query?: string): void {
  let currentPage = 1;

  const album = document.getElementById('film-container');
  if (album) {
    album.innerHTML = '';
  }

  const loadMoreContainer = document.querySelector('.load-more-container');
  if (loadMoreContainer) {
    loadMoreContainer.innerHTML = '';
  }

  const newLoadMoreBtn = createLoadMoreBtn(fetchMovies);
  loadMoreContainer?.appendChild(newLoadMoreBtn);

  const loadMoreBtn = document.getElementById('load-more');

  fetchMovies();

    function fetchMovies() {
      func(currentPage, query)
        .then((movie: IFetchResult) => {
          const {totalPages, movies} = movie;
          if (movies.length) {
            movies.map((movie) => {
              const { id } = movie;
              const classes = ['card-container', 'col-lg-3', 'col-md-4', 'col-12', 'p-2', `movie-${id}`]
              const card = createCard(movie, classes, likeBtnHandler);
              album?.appendChild(card)
            });

            if(currentPage < totalPages) {
              if (loadMoreBtn) {
                loadMoreBtn.style.display = 'block';
              }

              currentPage += 1;
            } else {
              if (loadMoreBtn) {
                loadMoreBtn.style.display = 'none';
              }
            }
          } else {
            const message = document.createElement('p');
            message.innerHTML = 'Nothing was found. Try another movie';
            album?.appendChild(message)
          }
        })
        .catch(err => console.log(err));
    }
}