import ApiService from "../apiService";

export default function renderRandomMovie(): void {
  const service = new ApiService();
  const randomMovieContainer = document.getElementById('random-movie');
  const movieName = document.getElementById('random-movie-name');
  const movieDescription = document.getElementById('random-movie-description');
  const quantityOfItemsOnPage = 20;
  const randomNumber = getRandomNumber(0, quantityOfItemsOnPage - 1);

  service.getMoviesForRandom()
    .then((movies) => {
      const randomMovie = movies[randomNumber];
      const { title, overview, backdrop } = randomMovie;
      if (randomMovieContainer) {
        randomMovieContainer.style.backgroundImage = `url(${backdrop})`;
      }

      if (movieName) {
        movieName.innerHTML = title;
      }


      if (movieDescription) {
        movieDescription.innerHTML = overview;
      }
    })
    .catch(err => console.log(err));

  function getRandomNumber(min: number, max: number) {
      const rand = min + Math.random() * (max + 1 - min);
      return Math.floor(rand);
  }
}