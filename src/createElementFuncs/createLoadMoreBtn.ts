export default function createLoadMoreBtn(handler: () => void): HTMLElement {
	const btn = document.createElement('button');
	btn.classList.add('btn', 'btn-lg', 'btn-outline-success', 'hidden');
	btn.id = 'load-more';
	btn.setAttribute('type', 'button');
	btn.style.display = 'none';
	btn.innerHTML = 'Load more';
	btn.addEventListener('click', handler)

	return btn
}