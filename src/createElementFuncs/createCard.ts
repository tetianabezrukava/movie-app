import { IMovie } from '../typingHelpers/interfaces';
import { HeartBtnHandler } from '../typingHelpers/types';

export default function createCard(movie: IMovie, classes: string[], handler: HeartBtnHandler): HTMLElement {
	const { id, overview, poster, release } = movie;
	const temp = document.getElementById(`card-body`) as HTMLTemplateElement;
	const tempContent = temp.content;
	const cardBody = document.importNode(tempContent, true);
	const card = document.createElement('div');

	classes.forEach(item => card.classList.add(item))
	card.appendChild(cardBody)

	const img = card.querySelector('img');
	const text = card.querySelector('.card-text');
	const releaseDate = card.querySelector('.text-muted');
	const btn = card.querySelector('svg');

	const likedMoviesArr = JSON.parse(localStorage.likedMovies);
	const stringId = id.toString();

	if (img) {
		img.src = poster || 'src/assets/default-poster.jpg';
	}

	if (text) {
		text.innerHTML = overview;
	}

	if (releaseDate) {
		releaseDate.innerHTML = release;
	}

	if(likedMoviesArr.includes(stringId)) {
		if (btn) {
			btn.style.fill = 'red';
		}
	}

	btn?.addEventListener('click', () => handler(stringId, btn));

	return card
}